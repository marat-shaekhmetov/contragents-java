package ru.company.contragents.web;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.company.contragents.model.Organization;
import ru.company.contragents.service.OrganizationService;

import java.util.List;

@RestController
public class OrganizationRest {

    @Autowired
    OrganizationService organizationService;

    @ApiOperation(value = "View a list of organization types", response = Organization.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of organization types"),
            @ApiResponse(code = 204, message = "no organization types currently")
    })
    @RequestMapping(value = "/organization-types", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<Organization>> getTypesOrganization() {
        List<Organization> organizations = organizationService.getTypesOrganization();

        if (organizations == null || organizations.size() == 0) {
            return new ResponseEntity<List<Organization>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Organization>>(organizations, HttpStatus.OK);
    }

}