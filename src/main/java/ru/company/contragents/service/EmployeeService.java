package ru.company.contragents.service;

import org.springframework.stereotype.Service;
import ru.company.contragents.model.Employee;

import java.util.List;

@Service
public interface EmployeeService {

    List<Employee> getEmployees();

}
