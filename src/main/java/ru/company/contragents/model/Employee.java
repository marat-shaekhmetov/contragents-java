package ru.company.contragents.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Employee {

    private Long id;

    @NotNull
    @Size(min=10, max=50)
    private String fullName;

    @NotNull
    @Size(min=5, max=20)
    private String phone;

    public Employee() {
    }

    public Employee(String fullName, String phone) {
        this.fullName = fullName;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;

        Employee employee = (Employee) o;

        if (!fullName.equals(employee.fullName)) return false;
        return phone.equals(employee.phone);

    }

    @Override
    public int hashCode() {
        int result = fullName.hashCode();
        result = 31 * result + phone.hashCode();
        return result;
    }
}

