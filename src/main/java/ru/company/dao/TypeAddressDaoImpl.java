package ru.company.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.company.entity.TypeAddress;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by user on 11.08.2017.
 */
@Repository
public class TypeAddressDaoImpl implements TypeAddressDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<TypeAddress> getTypeAddresses() {
        return entityManager.createNamedQuery("findAllTypeAddresses", TypeAddress.class).getResultList();
    }

    @Override
    @Transactional
    public void addTypeAddress(TypeAddress typeAddress) {
        entityManager.persist(typeAddress);
    }

    @Override
    @Transactional
    public void deleteTypeAddress(Long id) {
        TypeAddress typeAddress = entityManager.find(TypeAddress.class, id);
        entityManager.remove(typeAddress);
    }


}