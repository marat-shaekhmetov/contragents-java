package ru.company.dao;

import ru.company.entity.Contragent;

import java.util.List;

/**
 * Created by user on 07.08.2017.
 */
public interface ContragentDao {
    void addContragent(Contragent contragent);

    void updateContragent(Contragent contragent);

    void deleteContragent(long id);

    Contragent getContragent(long id);

    List<Contragent> getContragents();
}
