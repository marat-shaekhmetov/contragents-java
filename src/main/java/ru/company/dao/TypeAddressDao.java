package ru.company.dao;

import ru.company.entity.TypeAddress;

import java.util.List;

/**
 * Created by user on 11.08.2017.
 */
public interface TypeAddressDao {
    List<TypeAddress> getTypeAddresses();

    void addTypeAddress(TypeAddress typeAddress);

    void deleteTypeAddress(Long id);

}