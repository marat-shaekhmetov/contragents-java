package ru.company.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.company.entity.OrgType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by user on 07.08.2017.
 */
@Repository
public class OrgTypeDaoImpl implements OrgTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OrgType> getOrgTypes() {
        List<OrgType> orgTypes = entityManager.createNamedQuery("findAllOrgTypes", OrgType.class).getResultList();

        return orgTypes;
    }

    @Override
    public OrgType getOrgTypeById(Long id) {
        return entityManager.find(OrgType.class, id);
    }

    @Override
    @Transactional
    public void addOrgType(OrgType orgType) {
        entityManager.persist(orgType);
    }

    @Override
    @Transactional
    public void deleteOrgType(Long id) {
        OrgType orgType = entityManager.find(OrgType.class, id);
        entityManager.remove(orgType);
    }


}
