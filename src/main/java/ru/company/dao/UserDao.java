package ru.company.dao;

import ru.company.entity.User;

import java.util.List;

/**
 * Created by user on 07.08.2017.
 */
public interface UserDao {
    List<User> getUsers();

    User getUserById(Long id);

    void addUser(User user);

    void updateUser(User user);

    void deleteUser(long id);

}
