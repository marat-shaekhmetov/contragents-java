package ru.company.dao;

import org.springframework.stereotype.Repository;
import ru.company.entity.Address;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by user on 10.08.2017.
 */
@Repository
public class AddressDaoImpl implements AddressDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Address getAddressById(Long id) {
        return entityManager.find(Address.class, id);
    }
}
