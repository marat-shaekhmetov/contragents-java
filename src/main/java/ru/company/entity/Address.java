package ru.company.entity;

import javax.persistence.*;

/**
 * Created by user on 04.08.2017.
 */
@Embeddable
@Table(name = "ADDRESS")
public class Address {
    @Column(name = "TYPE_ADDRESS")
    private Long typeAddressId;
    @Column(name = "ADDRESS")
    private String address;

    public Address() {
    }

    public Address(Long typeAddressId, String address) {
        this.typeAddressId = typeAddressId;
        this.address = address;
    }

    public Long getTypeAddress() {
        return typeAddressId;
    }

    public void setTypeAddress(Long typeAddress) {
        this.typeAddressId = typeAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Address{" +
                "typeAddressId=" + typeAddressId +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address1 = (Address) o;

        if (!typeAddressId.equals(address1.typeAddressId))
            return false;
        return address.equals(address1.address);
    }

    @Override
    public int hashCode() {
        int result = typeAddressId != null ? typeAddressId.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}
