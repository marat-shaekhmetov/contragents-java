package ru.company.contragents.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.company.dao.UserDao;
import ru.company.entity.User;

import java.util.List;

/**
 * Created by user on 17.08.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "file:src/main/resources/META-INF/context.xml",
})
@SpringBootTest
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void testUserDao(){
        User user = new User();
        user.setName("User1");
        user.setPhone("123456");

        userDao.addUser(user);
        User user2 = userDao.getUserById(user.getId());
        Assert.assertEquals(user, user2);

        user.setName("newName");
        userDao.updateUser(user);
        Assert.assertEquals(user, userDao.getUserById(user.getId()));

        List<User> users = userDao.getUsers();
        userDao.deleteUser(user.getId());
        Assert.assertEquals(users.size()-userDao.getUsers().size(), 1);
    }
}
