package ru.company.contragents.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.company.dao.ContragentDao;
import ru.company.dao.OrgTypeDao;
import ru.company.dao.TypeAddressDao;
import ru.company.dao.UserDao;
import ru.company.entity.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17.08.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "file:src/main/resources/META-INF/context.xml",
})
@SpringBootTest
public class ContragentDaoTest {

    @Autowired
    ContragentDao contragentDao;
    @Autowired
    OrgTypeDao orgTypeDao;
    @Autowired
    UserDao userDao;
    @Autowired
    TypeAddressDao typeAddressDao;

    TypeAddress typeAddress1;
    TypeAddress typeAddress2;

    @Test
    public void testContragentDao(){
        Contragent contragent = createContragent();
        contragentDao.addContragent(contragent);
        Contragent contragent2 = contragentDao.getContragent(contragent.getId());
        Assert.assertEquals(contragent, contragent2);

        contragent.setName("new Name");
        contragentDao.updateContragent(contragent);
        Assert.assertEquals(contragent, contragentDao.getContragent(contragent.getId()));

        long userId = contragent.getUser();
        long orgTypeId = contragent.getOrgTypeId();
        List<Contragent> contragents = contragentDao.getContragents();
        contragentDao.deleteContragent(contragent.getId());
        Assert.assertEquals(contragents.size()-contragentDao.getContragents().size(), 1);

        userDao.deleteUser(userId);
        orgTypeDao.deleteOrgType(orgTypeId);

        typeAddressDao.deleteTypeAddress(typeAddress1.getId());
        typeAddressDao.deleteTypeAddress(typeAddress2.getId());

    }

    private Contragent createContragent(){
        Contragent contragent = new Contragent();
        contragent.setName("contragentName");
        contragent.setFullName("full name");
        contragent.setInn("123");
        contragent.setPhone("987456");
        contragent.setEmail("email@email.email");

        OrgType orgType = new OrgType();
        orgType.setName("orgTypeName");
        orgTypeDao.addOrgType(orgType);
        contragent.setOrgTypeId(orgType.getId());

        User user = new User();
        user.setName("User1");
        user.setPhone("123456");
        userDao.addUser(user);
        contragent.setUser(user.getId());

        typeAddress1 = new TypeAddress();
        typeAddress1.setName("type1");
        typeAddress2 = new TypeAddress();
        typeAddress2.setName("type2");

        typeAddressDao.addTypeAddress(typeAddress1);
        typeAddressDao.addTypeAddress(typeAddress2);

        ArrayList<Address> addresses = new ArrayList<>();
        addresses.add(new Address(typeAddress1.getId(), "street1"));
        addresses.add(new Address(typeAddress2.getId(), "street2"));
        contragent.setAddresses(addresses);

        ArrayList<ContactFace> contactFaces = new ArrayList<>();
        contactFaces.add(new ContactFace("name", "123456", "email"));
        contragent.setContactFaces(contactFaces);

        return contragent;
    }
}
