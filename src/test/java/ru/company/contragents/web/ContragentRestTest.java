package ru.company.contragents.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.company.contragents.ContragentsApplication;
import ru.company.contragents.model.*;
import ru.company.contragents.service.ContragentService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ContragentsApplication.class})
@WebAppConfiguration
public class ContragentRestTest {

    private MockMvc mockMvc;

    @Mock
    private ContragentService contragentServiceMock;

    @InjectMocks
    private ContragentRest contragentRest;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(contragentRest)
                .build();
    }

    @Test
    public void getContragents() throws Exception {
        Organization organization1 = new Organization("OAO");
        Employee employee1 = new Employee("Nikolai Nikolaev", "89884566");
        List<ContactPerson> contactPersons = new ArrayList<>();
        contactPersons.add(new ContactPerson("Andrey Andreev", "89874455", "andrey@mail.ru"));
        contactPersons.add(new ContactPerson("Andrey Nikolaev", "89874455", "nikolay@mail.ru"));
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("city Ufa str Gagarina 15", new AddressType("Organization address")));
        addresses.add(new Address("city Ufa str Lenina 15", new AddressType("Organization address")));
        Contragent contragent = new Contragent("OAO Ivan", "Ivan Ivanov", organization1, "333333", "89893344", "ivan@mail.ru", employee1, contactPersons, addresses);
        List<Contragent> contragents = new ArrayList<>();
        contragents.add(contragent);

        Organization organization2 = new Organization("OAO");
        Employee employee2 = new Employee("Nikolai Nikolaev", "89884566");
        List<ContactPerson> contactPersons2 = new ArrayList<>();
        contactPersons2.add(new ContactPerson("Andrey Andreev", "89874455", "andrey@mail.ru"));
        contactPersons2.add(new ContactPerson("Andrey Nikolaev", "89874455", "nikolay@mail.ru"));
        List<Address> addresses2 = new ArrayList<>();
        addresses2.add(new Address("city Ufa str Gagarina 15", new AddressType("Organization address")));
        addresses2.add(new Address("city Ufa str Lenina 15", new AddressType("Organization address")));
        Contragent contragent2 = new Contragent("OAO Ivan", "Ivan Ivanov", organization2, "333333", "89893344", "ivan@mail.ru", employee2, contactPersons2, addresses2);
        contragents.add(contragent2);

        when(contragentServiceMock.getContragents()).thenReturn(contragents);
        mockMvc.perform(get("/contragents").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json; charset=UTF-8"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].name", is("OAO Ivan")))
                .andExpect(jsonPath("$[0].fullName", is("Ivan Ivanov")))
                .andExpect(jsonPath("$[0].orgType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].orgType.name", is("OAO")))
                .andExpect(jsonPath("$[0].phone", is("89893344")))
                .andExpect(jsonPath("$[0].mail", is("ivan@mail.ru")))
                .andExpect(jsonPath("$[0].responsible.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].responsible.fullName", is("Nikolai Nikolaev")))
                .andExpect(jsonPath("$[0].responsible.phone", is("89884566")))
                .andExpect(jsonPath("$[0].contactPersons[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].contactPersons[0].fullName", is("Andrey Andreev")))
                .andExpect(jsonPath("$[0].contactPersons[0].phone", is("89874455")))
                .andExpect(jsonPath("$[0].contactPersons[0].email", is("andrey@mail.ru")))
                .andExpect(jsonPath("$[0].contactPersons[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].contactPersons[1].fullName", is("Andrey Nikolaev")))
                .andExpect(jsonPath("$[0].contactPersons[1].phone", is("89874455")))
                .andExpect(jsonPath("$[0].contactPersons[1].email", is("nikolay@mail.ru")))
                .andExpect(jsonPath("$[0]addresses.[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0]addresses.[0].address", is("city Ufa str Gagarina 15")))
                .andExpect(jsonPath("$[0]addresses.[0].addressType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0]addresses.[0].addressType.type", is("Organization address")))
                .andExpect(jsonPath("$[0]addresses.[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0]addresses.[1].address", is("city Ufa str Lenina 15")))
                .andExpect(jsonPath("$[0]addresses.[1].addressType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0]addresses.[1].addressType.type", is("Organization address")))
                .andExpect(jsonPath("$[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].name", is("OAO Ivan")))
                .andExpect(jsonPath("$[1].fullName", is("Ivan Ivanov")))
                .andExpect(jsonPath("$[1].orgType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].orgType.name", is("OAO")))
                .andExpect(jsonPath("$[1].phone", is("89893344")))
                .andExpect(jsonPath("$[1].mail", is("ivan@mail.ru")))
                .andExpect(jsonPath("$[1].responsible.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].responsible.fullName", is("Nikolai Nikolaev")))
                .andExpect(jsonPath("$[1].responsible.phone", is("89884566")))
                .andExpect(jsonPath("$[1].contactPersons[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].contactPersons[0].fullName", is("Andrey Andreev")))
                .andExpect(jsonPath("$[1].contactPersons[0].phone", is("89874455")))
                .andExpect(jsonPath("$[1].contactPersons[0].email", is("andrey@mail.ru")))
                .andExpect(jsonPath("$[1].contactPersons[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].contactPersons[1].fullName", is("Andrey Nikolaev")))
                .andExpect(jsonPath("$[1].contactPersons[1].phone", is("89874455")))
                .andExpect(jsonPath("$[1].contactPersons[1].email", is("nikolay@mail.ru")))
                .andExpect(jsonPath("$[1]addresses.[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1]addresses.[0].address", is("city Ufa str Gagarina 15")))
                .andExpect(jsonPath("$[1]addresses.[0].addressType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1]addresses.[0].addressType.type", is("Organization address")))
                .andExpect(jsonPath("$[1]addresses.[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1]addresses.[1].address", is("city Ufa str Lenina 15")))
                .andExpect(jsonPath("$[1]addresses.[1].addressType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1]addresses.[1].addressType.type", is("Organization address")));

        verify(contragentServiceMock, times(1)).getContragents();
        verifyNoMoreInteractions(contragentServiceMock);
    }

    @Test
    public void getNotExistContragents() throws Exception {
        when(contragentServiceMock.getContragents()).thenReturn(null);

        mockMvc.perform(get("/contragents").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isNoContent());

        verify(contragentServiceMock, times(1)).getContragents();
        verifyNoMoreInteractions(contragentServiceMock);
    }

    @Test
    public void getContragentById() throws Exception {
        Organization organization = new Organization("OAO");
        Employee employee = new Employee("Nikolai Nikolaev", "89884566");
        List<ContactPerson> contactPersons = new ArrayList<>();
        contactPersons.add(new ContactPerson("Andrey Andreev", "89874455", "andrey@mail.ru"));
        contactPersons.add(new ContactPerson("Andrey Nikolaev", "89874455", "nikolay@mail.ru"));
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("city Ufa str Gagarina 15", new AddressType("Organization address")));
        addresses.add(new Address("city Ufa str Lenina 15", new AddressType("Organization address")));
        Contragent contragent = new Contragent("OAO Ivan", "Ivan Ivanov", organization, "3333", "89893344", "ivan@mail.ru", employee, contactPersons, addresses);

        when(contragentServiceMock.getContragentById(1l)).thenReturn(contragent);

        mockMvc.perform(get("/contragents/1").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json; charset=UTF-8"))
                .andExpect(jsonPath("$.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.name", is("OAO Ivan")))
                .andExpect(jsonPath("$.fullName", is("Ivan Ivanov")))
                .andExpect(jsonPath("$.orgType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.orgType.name", is("OAO")))
                .andExpect(jsonPath("$.phone", is("89893344")))
                .andExpect(jsonPath("$.mail", is("ivan@mail.ru")))
                .andExpect(jsonPath("$.responsible.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.responsible.fullName", is("Nikolai Nikolaev")))
                .andExpect(jsonPath("$.responsible.phone", is("89884566")))
                .andExpect(jsonPath("$.contactPersons[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.contactPersons[0].fullName", is("Andrey Andreev")))
                .andExpect(jsonPath("$.contactPersons[0].phone", is("89874455")))
                .andExpect(jsonPath("$.contactPersons[0].email", is("andrey@mail.ru")))
                .andExpect(jsonPath("$.contactPersons[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.contactPersons[1].fullName", is("Andrey Nikolaev")))
                .andExpect(jsonPath("$.contactPersons[1].phone", is("89874455")))
                .andExpect(jsonPath("$.contactPersons[1].email", is("nikolay@mail.ru")))
                .andExpect(jsonPath("$.addresses.[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.addresses.[0].address", is("city Ufa str Gagarina 15")))
                .andExpect(jsonPath("$.addresses.[0].addressType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.addresses.[0].addressType.type", is("Organization address")))
                .andExpect(jsonPath("$.addresses.[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.addresses.[1].address", is("city Ufa str Lenina 15")))
                .andExpect(jsonPath("$.addresses.[1].addressType.id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.addresses.[1].addressType.type", is("Organization address")));

        verify(contragentServiceMock, times(1)).getContragentById(1l);
        verifyNoMoreInteractions(contragentServiceMock);
    }

    @Test
    public void getNotExistContragentById() throws Exception {
        when(contragentServiceMock.getContragentById(1l)).thenReturn(null);

        mockMvc.perform(get("/contragents/1").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isNoContent());

        verify(contragentServiceMock, times(1)).getContragentById(1l);
        verifyNoMoreInteractions(contragentServiceMock);
    }

    @Test
    public void addContragent() throws Exception {
        Organization organization = new Organization("OAO");
        Employee employee = new Employee("Nikolai Nikolaev", "89884566");
        List<ContactPerson> contactPersons = new ArrayList<>();
        contactPersons.add(new ContactPerson("Andrey Andreev", "89874455", "andrey@mail.ru"));
        contactPersons.add(new ContactPerson("Andrey Nikolaev", "89874455", "nikolay@mail.ru"));
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("city Ufa str Gagarina 15", new AddressType("Organization address")));
        addresses.add(new Address("city Ufa str Lenina 15", new AddressType("Organization address")));
        Contragent contragent = new Contragent("OAO Ivan", "Ivan Ivanov", organization, "333333", "89893344", "ivan@mail.ru", employee, contactPersons, addresses);
        contragent.setId(1l);

        ObjectMapper mapper = new ObjectMapper();
        String contragent_json = mapper.writeValueAsString(contragent);

        mockMvc.perform(post("/contragents")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(contragent_json))
                .andExpect(status().isCreated());

    }

    @Test
    public void updateContragent() throws Exception {
        Organization organization = new Organization("OAO");
        Employee employee = new Employee("Nikolai Nikolaev", "89884566");
        List<ContactPerson> contactPersons = new ArrayList<>();
        contactPersons.add(new ContactPerson("Andrey Andreev", "89874455", "andrey@mail.ru"));
        contactPersons.add(new ContactPerson("Andrey Nikolaev", "89874455", "nikolay@mail.ru"));
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("city Ufa str Gagarina 15", new AddressType("Organization address")));
        addresses.add(new Address("city Ufa str Lenina 15", new AddressType("Organization address")));
        Contragent contragent = new Contragent("OAO Ivan", "Ivan Ivanov", organization, "333333", "89893344", "ivan@mail.ru", employee, contactPersons, addresses);
        contragent.setId(1l);

        ObjectMapper mapper = new ObjectMapper();
        String contragent_json = mapper.writeValueAsString(contragent);

        when(contragentServiceMock.checkIfContragentExist(contragent.getId())).thenReturn(true);

        mockMvc.perform(put("/contragents/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(contragent_json))
                .andExpect(status().isOk());

        verify(contragentServiceMock, times(1)).checkIfContragentExist(contragent.getId());
    }

    @Test
    public void updateNotExistContragent() throws Exception {
        Organization organization = new Organization("OAO");
        Employee employee = new Employee("Nikolai Nikolaev", "89884566");
        List<ContactPerson> contactPersons = new ArrayList<>();
        contactPersons.add(new ContactPerson("Andrey Andreev", "89874455", "andrey@mail.ru"));
        contactPersons.add(new ContactPerson("Andrey Nikolaev", "89874455", "nikolay@mail.ru"));
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("city Ufa str Gagarina 15", new AddressType("Organization address")));
        addresses.add(new Address("city Ufa str Lenina 15", new AddressType("Organization address")));
        Contragent contragent = new Contragent("OAO Ivan", "Ivan Ivanov", organization, "333333", "89893344", "ivan@mail.ru", employee, contactPersons, addresses);
        contragent.setId(1l);

        ObjectMapper mapper = new ObjectMapper();
        String contragent_json = mapper.writeValueAsString(contragent);

        when(contragentServiceMock.checkIfContragentExist(contragent.getId())).thenReturn(false);

        mockMvc.perform(put("/contragents/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(contragent_json))
                .andExpect(status().isNoContent());

        verify(contragentServiceMock, times(1)).checkIfContragentExist(contragent.getId());
    }

    @Test
    public void deleteContragent() throws Exception {
        when(contragentServiceMock.checkIfContragentExist(1l)).thenReturn(true);
        doNothing().when(contragentServiceMock).deleteContragent(1l);

        mockMvc.perform(delete("/contragents/1"))
                .andExpect(status().isOk());

        verify(contragentServiceMock, times(1)).checkIfContragentExist(1l);
        verify(contragentServiceMock, times(1)).deleteContragent(1l);
        verifyNoMoreInteractions(contragentServiceMock);
    }

    @Test
    public void deleteNotExistContragent() throws Exception {
        when(contragentServiceMock.checkIfContragentExist(1l)).thenReturn(false);
        doNothing().when(contragentServiceMock).deleteContragent(1l);

        mockMvc.perform(delete("/contragents/1"))
                .andExpect(status().isNoContent());

        verify(contragentServiceMock, times(1)).checkIfContragentExist(1l);
        verifyNoMoreInteractions(contragentServiceMock);
    }
}
