package ru.company.contragents;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.company.dao.*;
import ru.company.entity.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration({
		"file:src/main/resources/META-INF/context.xml",
})
@SpringBootTest
public class ContragentsApplicationTests {

	@Test
	public void contextLoads() {

	}

}
